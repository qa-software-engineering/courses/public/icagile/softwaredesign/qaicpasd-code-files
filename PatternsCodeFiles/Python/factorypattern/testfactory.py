import accountfactory as af

current = af.AccountFactory.get_account("current", 250, "My Current Account", 100)
savings = af.AccountFactory.get_account("SAVINGS", 300, "My Savings Account", 0)
print(f"Factory Current Account Details::{current}")
print(f"Factory Savings Account Details::{savings}")

# Try to Withdraw within Current account limit
current.withdraw(300)
print(f"Factory Current Account Details::{current}")

# Try to go over Current overdraft limit
current.withdraw(300)
print(f"Factory Current Account Details::{current}")

# Try to Withdraw more than balance
savings.withdraw(350)
print(f"Factory Savings Account Details::{savings}")


# Try to Withdraw less than balance
savings.withdraw(200)
print(f"Factory Savings Account Details::{savings}")

