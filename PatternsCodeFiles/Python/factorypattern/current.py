import account as acc

class Current(acc.Account):

	def __init__(self, balance, name, overdraft_limit):
		super().__init__(balance, name)
		self.__overdraft_limit = overdraft_limit

	@property
	def overdraft_limit(self):
		return self.__overdraft_limit

	def withdraw(self, amt):
		if amt < self.balance + self.overdraft_limit:
			super().withdraw(amt)
			return True
		return False

	def __str__(self):
		return f"BALANCE={self.balance}, NAME={self.name}, OVERDRAFT LIMIT ={self.overdraft_limit}"