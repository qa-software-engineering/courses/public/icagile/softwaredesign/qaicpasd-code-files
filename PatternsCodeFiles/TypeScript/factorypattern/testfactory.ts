import {AccountFactory} from "./accountfactory"

let current = AccountFactory.get_account("current", 250, "My Current Account", 100);
let savings = AccountFactory.get_account("SAVINGS", 300, "My Savings Account", 0);
console.log("Factory Current Account Details::" + current)
console.log("Factory Savings Account Details::{savings}")

// Try to Withdraw within Current account limit
current.withdraw(300)
console.log("Factory Current Account Details::"+current)

// Try to go over Current overdraft limit
current.withdraw(300)
console.log("Factory Current Account Details::"+current)

// Try to Withdraw more than balance
savings.withdraw(350)
console.log("Factory Savings Account Details::"+savings)


// Try to Withdraw less than balance
savings.withdraw(200)
console.log("Factory Savings Account Details::"+savings)

