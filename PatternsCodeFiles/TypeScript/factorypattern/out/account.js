"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Account = /** @class */ (function () {
    function Account(balance, name) {
        this._balance = balance;
        this._name = name;
    }
    Object.defineProperty(Account.prototype, "balance", {
        get: function () {
            return this._balance;
        },
        set: function (value) {
            this._balance = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Account.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Account.prototype.withdraw = function (amt) {
        this._balance -= amt;
    };
    Account.prototype.toString = function () {
        return "BALANCE=" + this.balance + ",NAME=" + this.name;
    };
    Account.prototype.deposit = function (amt) {
        this.balance += amt;
        return true;
    };
    return Account;
}());
exports.Account = Account;
