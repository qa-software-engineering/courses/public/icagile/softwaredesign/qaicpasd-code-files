import {Account} from "./account"

export class Savings extends Account
{

    withdraw(amt):boolean
    {
        if (amt < this.balance)
        {

            super.withdraw(amt);
            return true;
        }
        return false;
    }
}
