using System;
namespace FactoryPattern
{
	public abstract class Account {

		protected int balance;
		protected String name;

		public int Balance { get => balance; private set => balance = value; }
		public string Name { get => name; private set => name = value; }

		public abstract bool Withdraw(int amt);

		


		
		public override String ToString() {
			return "BALANCE=" + this.Balance + ", NAME=" + this.Name;
		}

		public bool Deposit(int amt) {
			this.Balance += amt;
			return true;
		}

	}
}