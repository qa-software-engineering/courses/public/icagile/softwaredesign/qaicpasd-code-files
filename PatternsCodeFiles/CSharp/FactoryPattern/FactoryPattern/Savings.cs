using System;
namespace FactoryPattern
{
    public class Savings : Account
    {

        public Savings(int balance, String name)
        {
            this.balance = balance;
            this.name = name;
        }


        public override bool Withdraw(int amt)
        {
            if (amt < this.balance)
            {
                this.balance -= amt;
                return true;
            }
            return false;
        }

    }
}