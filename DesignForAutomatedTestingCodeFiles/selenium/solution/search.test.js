require("chromedriver");
const { Builder, By, Key } = require("selenium-webdriver");

describe(`DuckDuckGo search string tests`, () => {

  const searchString = `Automation testing with Selenium and JavaScript`;
  let driver;

  beforeEach(async () => {

    try {
      // Get selenium to open Chrome
      driver = await new Builder().forBrowser("chrome").build();
    }
    catch {
      error => console.log(error);
    }
  });

  afterEach(async () => {
    // Clear the driver from the current test
    await driver?.quit();
  });

  test(`returns the correct page title when RETURN is press after text input`, async () => {

    // Arrange
    // Done in beforeEach

    // Act
    // Get Selenium to go to DuckDuckGo
    await driver.get("http://duckduckgo.com");
    // Get Selenium to type in the search box and press enter
    await driver.findElement(By.name("q")).sendKeys(searchString, Key.RETURN);

    // Assert
    // Expect the page title to contain searchString
    expect(await driver.getTitle()).toContain(searchString);

    // Clean Up
    // Done in afterEach

  })

  test(`returns the correct page title when the search button is pressed after text input`, async () => {

    // Arrange
    // Done in beforeEach()


    // Act 
    // Get Selenium to open Chrome and go to DuckDuckGo
    await driver.get("http://duckduckgo.com");
    // Get Selenium to find the search box and type the searchString into it
    await driver.findElement(By.name("q")).sendKeys(searchString);
    // Get Selenium to find the search button and click it
    await driver.findElement(By.id("search_button_homepage")).click();

    // Assert
    // Expect the page title to contain searchString
    expect(await driver.getTitle()).toContain(searchString);

    // Clean Up
    // Done in the afterEach

  });
});
