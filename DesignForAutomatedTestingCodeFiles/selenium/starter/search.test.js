require("chromedriver");
const { Builder, By, Key } = require("selenium-webdriver");

describe(`DuckDuckGo search string tests`, () => {

  const searchString = `Automation testing with Selenium and JavaScript`;
  let driver;

  beforeEach(async () => {
    try {
      driver = await new Builder().forBrowser("chrome").build();
    }
    catch {
      error => console.log(error);
    }
  });

  afterEach(async () => {
    // Clear the driver from the current test
    await driver?.quit();
  });

  test(`returns the correct page title when RETURN is press after text input`, async () => {

    // Arrange
    // Done in beforeEach

    // Act
    // Get Selenium to go to DuckDuckGo

    // Get Selenium to type in the search box and press enter


    // Assert
    // Expect the page title to contain searchString


    // Clean Up
    // Done in  afterEach

  })

});
